/******************************************************************
 *   © 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   mbnodecontrolleralglib.cpp
 * 
 ******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <ap.h>
#include <alglibinternal.h>
#include <alglibmisc.h>
#include <diffequations.h>
#include <dataanalysis.h>
#include <fasttransforms.h>
#include <integration.h>
#include <interpolation.h>
#include <linalg.h>
#include <optimization.h>
#include <solvers.h>
#include <specialfunctions.h>
#include <statistics.h>
#include <stdafx.h>

#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <chrono>

#include <stdint.h> 
#include <unistd.h>      
#include <time.h> 


#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"

#include "rosgraph_msgs/Clock.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include "nav_msgs/Path.h"




// variabili per memorizzare traiettoria in memoria
int flagMemorizzaTraiettoria =0;
const int maxElem = 5000;
std::vector<double> MemX(maxElem);
std::vector<double> MemY(maxElem);
int Elem = 0;
std::vector<double> TargetFinale (3);

// file per traiettoria
std::ofstream fTraiettoria;

// file tempi di ottimizzazione
std::ofstream FtempiOttimizzazione;

// file tempi di CPU
std::ofstream FtempiCPU;

// file durata percorso
int ho_iniziato = 0;
std::ofstream FdurataPercorso;
auto inizioPercorso = std::chrono::steady_clock::now();
auto finePercorso = std::chrono::steady_clock::now();

// file distanza percorsa
std::ofstream FdistanzaPercorsa;
double G_distanza_percorsa = 0;

// flag  se in trajectory following flagTF=1
int flagTF =0;

// primo_target
bool primo_target=true;

// flag inizio controllore
int flagOpt=0;

//----DatiPosizione--------------------------------------
Eigen::VectorXd DPxk(2700);
int ScorriDPxk=3;
Eigen::VectorXd tempxk(2700);

//----DatiOstacoli
double ro= 0.12;
std::vector <double> xo (4);
std::vector <double> yo (4);


//-----VarGlobali---------------------------------------------
double pi_ = 3.14159265358;

std::vector<double> G_posizione_iniziale (3);

std::vector<double> G_vel_precedente {0,0};

double G_C1, G_C2, G_C3, G_C4, G_C5, G_C6, G_C7 =0;
std::vector<double> G_C_h {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


//dichiaro funzioni per funzioni di costo
Eigen::VectorXd mobRobCT0(Eigen::VectorXd x, Eigen::VectorXd u);
Eigen::VectorXd mobRobDT0(Eigen::VectorXd xk, Eigen::VectorXd uk, double Ts);
double mobRobCostFCN(Eigen::VectorXd du, Eigen::VectorXd x0, double Ts, int N, int M, Eigen::MatrixXd xref, Eigen::VectorXd u1);
double FunzioneDiCosto(Eigen::VectorXd du);


using namespace alglib;
// implementazione funzione oggetto vincoli e gradienti alglib
void nlcfunc2_jac(const real_1d_array &du, real_1d_array &fi, real_2d_array &jac, void *ptr)
{
  Eigen::VectorXd duE(6);
  duE << du[0], du[1], du[2], du[3], du[4], du[5];

  //inserisco fdc
  fi[0] = FunzioneDiCosto(duE);

  double h = 0.001;
  // derivata approssimata rispetto a du(0)    
  double FC = FunzioneDiCosto(duE);
  duE(0) = duE(0) + h;
  double FC_h = FunzioneDiCosto(duE);
  double derivata_approssimata_u11 = (FC_h - FC) / h;
  duE(0) = duE(0) - h;
  // derivata approssimata rispetto a du(1)  
  duE(1) = duE(1) + h;
  FC_h = FunzioneDiCosto(duE);
  double derivata_approssimata_u12 = (FC_h - FC) / h;
  duE(1) = duE(1) - h;
  // derivata approssimata rispetto a du(2)  
  duE(2) = duE(2) + h;
  FC_h = FunzioneDiCosto(duE);
  double derivata_approssimata_u21 = (FC_h - FC) / h;
  duE(2) = duE(2) - h;
  // derivata approssimata rispetto a du(3)  
  duE(3) = duE(3) + h;
  FC_h = FunzioneDiCosto(duE);
  double derivata_approssimata_u22 = (FC_h - FC) / h;
  duE(3) = duE(3) - h;
  // derivata approssimata rispetto a du(4)  
  duE(4) = duE(4) + h;
  FC_h = FunzioneDiCosto(duE);
  double derivata_approssimata_u31 = (FC_h - FC) / h;
  duE(4) = duE(4) - h;
  // derivata approssimata rispetto a du(5)  
  duE(5) = duE(5) + h;
  FC_h = FunzioneDiCosto(duE);
  duE(5) = duE(5) - h;
  double derivata_approssimata_u32 = (FC_h - FC) / h;


  // raggio ruote e mezzo interasse 
  double r = 0.020;
  double d = 0.053 / 2;

  // inizializzazione vettore dei vincoli
  Eigen::VectorXd g(10);

  Eigen::MatrixXd M(6, 6);
  M.setZero(6, 6);
  M(0, 0) = 1 / r;
  M(0, 1) = d / r;
  M(1, 0) = 1 / r;
  M(1, 1) = -d / r;
  M(2, 2) = 1 / r;
  M(2, 3) = d / r;
  M(3, 2) = 1 / r;
  M(3, 3) = -d / r;
  M(4, 4) = 1 / r;
  M(4, 5) = d / r;
  M(5, 4) = 1 / r;
  M(5, 5) = -d / r;
  double v1 = G_vel_precedente[0] + duE(0);
  double w1 = G_vel_precedente[1] + duE(1);
  double v2 = G_vel_precedente[0] + duE(0) + duE(2);
  double w2 = G_vel_precedente[1] + duE(1) + duE(3);
  double v3 = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
  double w3 = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);
  Eigen::VectorXd u(6);
  u << v1, w1, v2, w2, v3, w3;
  Eigen::VectorXd temp(6);
  temp = M * u;
  // vincoli di velocità
  for (int i = 0; i < 6; i++)
  {
    g(i) = abs(temp(i));
  }

  // vincoli di posizione
  Eigen::VectorXd xin(3);
  xin << G_posizione_iniziale[0], G_posizione_iniziale[1], G_posizione_iniziale[2];
  double T = 0.1;

  Eigen::VectorXd uk1(2);
  uk1 << v1, w1;
  Eigen::VectorXd uk2(2);
  uk2 << v2, w2;
  Eigen::VectorXd uk3(2);
  uk3 << v3, w3;

  Eigen::VectorXd xk1(3);
  xk1 = mobRobDT0(xin, uk1, T);
  Eigen::VectorXd xk2(3);
  xk2 = mobRobDT0(xk1, uk2, T);
  Eigen::VectorXd xk3(3);
  xk3 = mobRobDT0(xk2, uk3, T);
  Eigen::VectorXd xk4(3);
  xk4 = mobRobDT0(xk3, uk3, T);
  Eigen::VectorXd xk5(3);
  xk5 = mobRobDT0(xk4, uk3, T);
  Eigen::VectorXd xk6(3);
  xk6 = mobRobDT0(xk5, uk3, T);
  Eigen::VectorXd xk7(3);
  xk7 = mobRobDT0(xk6, uk3, T);
  Eigen::VectorXd xk8(3);
  xk8 = mobRobDT0(xk7, uk3, T);
  Eigen::VectorXd xk9(3);
  xk9 = mobRobDT0(xk8, uk3, T);
  Eigen::VectorXd xk10(3);
  xk10 = mobRobDT0(xk9, uk3, T);
  Eigen::VectorXd xk11(3);
  xk11 = mobRobDT0(xk10, uk3, T);
  Eigen::VectorXd xk12(3);
  xk12 = mobRobDT0(xk11, uk3, T);

  double x12 = xk12(0);
  double y12 = xk12(1);

  double C1 = sqrt((y12 - yo[0]) * (y12 - yo[0]) + (x12 - xo[0]) * (x12 - xo[0]));
  double C2 = sqrt((y12 - yo[1]) * (y12 - yo[1]) + (x12 - xo[1]) * (x12 - xo[1]));
  double C3 = sqrt((y12 - yo[2]) * (y12 - yo[2]) + (x12 - xo[2]) * (x12 - xo[2]));
  double C4 = sqrt((y12 - yo[3]) * (y12 - yo[3]) + (x12 - xo[3]) * (x12 - xo[3]));

  G_C1 = C1;
  G_C2 = C2;
  G_C3 = C3;
  G_C4 = C4;

  G_C_h.clear();

  for (int j = 0; j < 6; j++)
  {
    duE(j) = duE(j) + 0.001;

    double v1_h = G_vel_precedente[0] + duE(0);
    double w1_h = G_vel_precedente[1] + duE(1);
    double v2_h = G_vel_precedente[0] + duE(0) + duE(2);
    double w2_h = G_vel_precedente[1] + duE(1) + duE(3);
    double v3_h = G_vel_precedente[0] + duE(0) + duE(2) + duE(4);
    double w3_h = G_vel_precedente[1] + duE(1) + duE(3) + duE(5);

    Eigen::VectorXd uk1_h(2);
    uk1_h << v1_h, w1_h;
    Eigen::VectorXd uk2_h(2);
    uk2_h << v2_h, w2_h;
    Eigen::VectorXd uk3_h(2);
    uk3_h << v3_h, w3_h;

    Eigen::VectorXd xk1_h(3);
    xk1_h = mobRobDT0(xin, uk1_h, T);
    Eigen::VectorXd xk2_h(3);
    xk2_h = mobRobDT0(xk1_h, uk2_h, T);
    Eigen::VectorXd xk3_h(3);
    xk3_h = mobRobDT0(xk2_h, uk3_h, T);
    Eigen::VectorXd xk4_h(3);
    xk4_h = mobRobDT0(xk3_h, uk3_h, T);
    Eigen::VectorXd xk5_h(3);
    xk5_h = mobRobDT0(xk4_h, uk3_h, T);
    Eigen::VectorXd xk6_h(3);
    xk6_h = mobRobDT0(xk5_h, uk3_h, T);
    Eigen::VectorXd xk7_h(3);
    xk7_h = mobRobDT0(xk6_h, uk3_h, T);
    Eigen::VectorXd xk8_h(3);
    xk8_h = mobRobDT0(xk7_h, uk3_h, T);
    Eigen::VectorXd xk9_h(3);
    xk9_h = mobRobDT0(xk8_h, uk3_h, T);
    Eigen::VectorXd xk10_h(3);
    xk10_h = mobRobDT0(xk9_h, uk3_h, T);
    Eigen::VectorXd xk11_h(3);
    xk11_h = mobRobDT0(xk10_h, uk3_h, T);
    Eigen::VectorXd xk12_h(3);
    xk12_h = mobRobDT0(xk11_h, uk3_h, T);

    double x12_h = xk12_h(0);
    double y12_h = xk12_h(1);

    double C1_h = sqrt((y12_h - yo[0]) * (y12_h - yo[0]) + (x12_h - xo[0]) * (x12_h - xo[0]));
    double C2_h = sqrt((y12_h - yo[1]) * (y12_h - yo[1]) + (x12_h - xo[1]) * (x12_h - xo[1]));
    double C3_h = sqrt((y12_h - yo[2]) * (y12_h - yo[2]) + (x12_h - xo[2]) * (x12_h - xo[2]));
    double C4_h = sqrt((y12_h - yo[3]) * (y12_h - yo[3]) + (x12_h - xo[3]) * (x12_h - xo[3]));

    duE(j) = duE(j) - 0.001;

    G_C_h.push_back(C1_h);
    G_C_h.push_back(C2_h);
    G_C_h.push_back(C3_h);
    G_C_h.push_back(C4_h);
  }

  Eigen::VectorXd temp1(4);
  temp1 << C1, C2, C3, C4;

  for (int i = 6; i < 10; i++)
  {
    g(i) = temp1(i - 6);
  }

  fi[1] = g(0) - 1.5 * pi_;
  fi[2] = g(1) - 1.5 * pi_;
  fi[3] = g(2) - 1.5 * pi_;
  fi[4] = g(3) - 1.5 * pi_;
  fi[5] = g(4) - 1.5 * pi_;
  fi[6] = g(5) - 1.5 * pi_;
  fi[7] = ro - g(6);
  fi[8] = ro - g(7);
  fi[9] = ro - g(8);
  fi[10] = ro - g(9);

  // gradiente FDC
  jac[0][0] = derivata_approssimata_u11;
  //std::cout << "jac 0 0 =" << std::to_string(jac[0][0]) << std::endl;
  jac[0][1] = derivata_approssimata_u12;
  //std::cout << "jac 0 1 =" << std::to_string(jac[0][1]) << std::endl;
  jac[0][2] = derivata_approssimata_u21;
  //std::cout << "jac 0 2 =" << std::to_string(jac[0][2]) << std::endl;
  jac[0][3] = derivata_approssimata_u22;
  //std::cout << "jac 0 3 =" << std::to_string(jac[0][3]) << std::endl;
  jac[0][4] = derivata_approssimata_u31;
  //std::cout << "jac 0 4 =" << std::to_string(jac[0][4]) << std::endl;
  jac[0][5] = derivata_approssimata_u32;
  //std::cout << "jac 0 5 =" << std::to_string(jac[0][5]) << std::endl;

  // gradiente vincoli velocità
  jac[1][0] = 1 / r;
  jac[1][1] = d / r;
  jac[1][2] = 0.0;
  jac[1][3] = 0.0;
  jac[1][4] = 0.0;
  jac[1][5] = 0.0;

  jac[2][0] = 1 / r;
  jac[2][1] = -d / r;
  jac[2][2] = 0.0;
  jac[2][3] = 0.0;
  jac[2][4] = 0.0;
  jac[2][5] = 0.0;

  jac[3][0] = 1 / r;
  jac[3][1] = d / r;
  jac[3][2] = 1 / r;
  jac[3][3] = d / r;
  jac[3][4] = 0.0;
  jac[3][5] = 0.0;

  jac[4][0] = 1 / r;
  jac[4][1] = -d / r;
  jac[4][2] = 1 / r;
  jac[4][3] = -d / r;
  jac[4][4] = 0.0;
  jac[4][5] = 0.0;

  jac[5][0] = 1 / r;
  jac[5][1] = d / r;
  jac[5][2] = 1 / r;
  jac[5][3] = d / r;
  jac[5][4] = 1 / r;
  jac[5][5] = d / r;

  jac[6][0] = 1 / r;
  jac[6][1] = -d / r;
  jac[6][2] = 1 / r;
  jac[6][3] = -d / r;
  jac[6][4] = 1 / r;
  jac[6][5] = -d / r;

  // gradiente vincoli posizione
  jac[7][0] = -(G_C_h[0] - G_C1) / h;
  jac[7][1] = -(G_C_h[4] - G_C1) / h;
  jac[7][2] = -(G_C_h[8] - G_C1) / h;
  jac[7][3] = -(G_C_h[12] - G_C1) / h;
  jac[7][4] = -(G_C_h[16] - G_C1) / h;
  jac[7][5] = -(G_C_h[20] - G_C1) / h;

  jac[8][0] = -(G_C_h[1] - G_C2) / h;
  jac[8][1] = -(G_C_h[5] - G_C2) / h;
  jac[8][2] = -(G_C_h[9] - G_C2) / h;
  jac[8][3] = -(G_C_h[13] - G_C2) / h;
  jac[8][4] = -(G_C_h[17] - G_C2) / h;
  jac[8][5] = -(G_C_h[21] - G_C2) / h;

  jac[9][0] = -(G_C_h[2] - G_C3) / h;
  jac[9][1] = -(G_C_h[6] - G_C3) / h;
  jac[9][2] = -(G_C_h[10] - G_C3) / h;
  jac[9][3] = -(G_C_h[14] - G_C3) / h;
  jac[9][4] = -(G_C_h[18] - G_C3) / h;
  jac[9][5] = -(G_C_h[22] - G_C3) / h;

  jac[10][0] = -(G_C_h[3] - G_C4) / h;
  jac[10][1] = -(G_C_h[7] - G_C4) / h;
  jac[10][2] = -(G_C_h[11] - G_C4) / h;
  jac[10][3] = -(G_C_h[15] - G_C4) / h;
  jac[10][4] = -(G_C_h[19] - G_C4) / h;
  jac[10][5] = -(G_C_h[23] - G_C4) / h;
}

class Controller
{
public:
  Controller(ros::NodeHandle &nh);
  // LABORATORIO
  //void ePuck_poseCallbackTEST(const geometry_msgs::PoseStamped trans);
  void ePuck_poseCallbackTEST(const geometry_msgs::TransformStamped trans);
  void clockCallbackTEST(const rosgraph_msgs::Clock rclock);
  void timer1CallbackTEST(const ros::TimerEvent &);
  void targetCallback(const geometry_msgs::Pose2D target );
  void trajectoryCallback(const nav_msgs::Path trajectory);
 
protected:
  ros::Publisher commandPub;
  ros::Time robot_clock;
};
 
Controller::Controller(ros::NodeHandle &nh)
{
  // LABORATORIO
  // commandPub = nh.advertise<geometry_msgs::Twist>("/epuck_robot_4/mobile_base/cmd_vel", 10);
  commandPub = nh.advertise<geometry_msgs::Twist>("ePuck2/cmd_vel", 10);
  ROS_INFO("..creata istanza controller");
};

void Controller::timer1CallbackTEST(const ros::TimerEvent &)
{
  geometry_msgs::Twist msg;

  // du iniziale da cui partire ad iterare
  real_1d_array du0 = "[0.2,-0.2,0,0,0,0]";

  // fornisco valore step
  real_1d_array s = "[1,1,1,1,1,1]";

  ae_int_t maxits = 0;
  minnlcstate state;
  minnlcreport rep;

  // inizializzazione vettore du opt
  real_1d_array du1;

  minnlccreate(6, du0, state);

  minnlcsetcond(state, 0, 0, 0, maxits);

  minnlcsetscale(state, s);

  double rho = 2000.0;
  ae_int_t outerits = 7;

  // setto ottimizzatore auglag
  minnlcsetalgoaul(state, rho, outerits);

  // setto limiti inferiori e superiori delle variabili di ottimizzazione
  real_1d_array bndl = "[-0.2,-0.2,-0.2,-0.2,-0.2,-0.2]";
  real_1d_array bndu = "[0.2,0.2,0.2,0.2,0.2,0.2]";
  minnlcsetbc(state, bndl, bndu);

  // setto i constraint
  minnlcsetnlc(state, 0, 10);

  // incremento del contatore per il vector DPxk, inserito qui per seguire il looprate
  if (ScorriDPxk < ((DPxk.size()) - 3))
  {
    ScorriDPxk = ScorriDPxk + 3;
  }

  // aggiungo raggiungimento della posizione iniziale prima del trajectory following
  double dist_pos_in = sqrt((DPxk(ScorriDPxk)-G_posizione_iniziale[0])*(DPxk(ScorriDPxk)-G_posizione_iniziale[0])+(DPxk(ScorriDPxk+1)-G_posizione_iniziale[1])*(DPxk(ScorriDPxk+1)-G_posizione_iniziale[1]));
  if(flagTF==1){
    if(dist_pos_in<=0.10){
      ScorriDPxk=0;
      DPxk=tempxk;
      flagTF=0;
    }
  }

  // flag partenza ottimizzazione
  if (flagOpt == 1)
  {
    if (ho_iniziato == 0)   {
       ho_iniziato = 1;
       inizioPercorso = std::chrono::steady_clock::now();
    }

    // inizio misura tempo di ottimizzazione
    auto start = std::chrono::steady_clock::now();
    // inizio misura utilizzo CPU
    clock_t ct1, ct2;  
    if ((ct1 = clock ()) == -1)  
        perror ("clock");
    // ottimizzazione
    alglib::minnlcoptimize(state, nlcfunc2_jac);
    // fine misura utilizzo CPU
    if ((ct2 = clock ()) == -1)      
        perror ("clock");
    FtempiCPU << ct2 - ct1 << "\n";

    //fine misura tempo di ottimizzazione
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    FtempiOttimizzazione << elapsed_seconds.count() << "\n";

    //prendo i risultati
    minnlcresults(state, du1, rep);

    Eigen::VectorXd duoptE(6);
    duoptE << du1[0], du1[1], du1[2], du1[3], du1[4], du1[5];

    msg.linear.x = G_vel_precedente[0] + du1[0];
    msg.angular.z = G_vel_precedente[1] + du1[1];
  }
  else if (flagOpt == 0)
  {
    msg.linear.x = 0;
    msg.angular.z = 0;
    G_vel_precedente[0] = msg.linear.x;
    G_vel_precedente[1] = msg.angular.z;
  }

  G_vel_precedente[0] = msg.linear.x;
  G_vel_precedente[1] = msg.angular.z;

  // memorizzo traiettoria in memoria
  if (flagMemorizzaTraiettoria==1){
    MemX[Elem]=G_posizione_iniziale[0];
    MemY[Elem]=G_posizione_iniziale[1];
    //std::cout<<"..memorizzo posizione traiettoria!"<<std::endl;
    double deltaAng = abs(G_posizione_iniziale[2]-TargetFinale[2]);
    double delta = sqrt((G_posizione_iniziale[0]-TargetFinale[0])*(G_posizione_iniziale[0]-TargetFinale[0])+(G_posizione_iniziale[1]-TargetFinale[1])*(G_posizione_iniziale[1]-TargetFinale[1]));
    //std::cout<<"delta = "<<std::to_string(delta)<<" deltaAng = "<<std::to_string(deltaAng)<<std::endl;
    if(delta <= 0.10 && deltaAng <= 0.05){
       finePercorso  = std::chrono::steady_clock::now();   
       std::chrono::duration<double> durata_percorso_in_secondi = finePercorso - inizioPercorso;
       FdurataPercorso << durata_percorso_in_secondi.count() << "\n";
       FdistanzaPercorsa << std::to_string(G_distanza_percorsa) << "\n";
       fTraiettoria.open ("OUT_alglib_traiettoria.csv");
       flagMemorizzaTraiettoria=0;
       int k = 0;
       for (k=0; k<=Elem; k++){
          fTraiettoria << MemX[k] << ";" << MemY[k] << "\n";
          std::cout<<">>>>>>>>>>>>> SCRIVO RIGA TRAIETTORIA!!!!"<<std::endl;
       }
       fTraiettoria.close();
    }
    Elem = Elem+1;
  }
  // pubblicazione comandi di velocità
  commandPub.publish(msg);
}

void Controller::ePuck_poseCallbackTEST(const geometry_msgs::TransformStamped trans)
{
  // LABORATORIO
  // double x = trans.pose.position.x;
  // double y = trans.pose.position.y;
  // double z = trans.pose.orientation.z;

  double x = trans.transform.translation.x;
  double y = trans.transform.translation.y; 
  double z = trans.transform.rotation.z;
  double w = trans.transform.rotation.w;

  double teta=0;
  
  teta = (2*asin(z));
  if(teta>-pi_/2 && teta< pi_/2){
    teta= -teta;
  }else if (teta > pi_/2 && teta <pi_ && w>0){
    teta=teta;
  }else if (teta >= pi_/2 && teta <pi_ && w<0){
    teta= -teta;
  }

  // std::cout<<"x="<<std::to_string(x)<<" "<<"y="<<std::to_string(y)<<" "<<"teta="<<std::to_string(teta)<<std::endl;
 
   if(ho_iniziato==1 && flagMemorizzaTraiettoria==1 ){
     double tmp = sqrt((G_posizione_iniziale[0]-x)*(G_posizione_iniziale[0]-x)+(G_posizione_iniziale[1]-y)*(G_posizione_iniziale[1]-y));
     G_distanza_percorsa= G_distanza_percorsa+tmp;
   }
 
  double rx = ((rand() % 6)-3)/100;
  double ry = ((rand() % 6)-3)/100;
  double rteta = ((rand() % 6)-3)/100;
  //std::cout<<"x="<<std::to_string(x)<<" "<<"y="<<std::to_string(y)<<" "<<"teta="<<std::to_string(teta)<<std::endl;
  x= x + rx;
  y= y + ry;
  teta= teta + rteta;

  //passaggio posizione a vector globale
  std::vector<double> v_pose ={x,y,teta};
  G_posizione_iniziale = v_pose;
  
  // ostacoli scena 1
  xo={1.10, 1.20, 0.40, 0.85};
  yo={0.82, 0.55, 0.30, 0.65}; 
  // ostacoli scena 2
  // xo={0.50, 1.00, 1.00, 1.00};
  // yo={0.50,0.25, 0.50, 0.75};
}

//intercettazione posizione desiderata
void Controller::targetCallback(const geometry_msgs::Pose2D target){
  ScorriDPxk=0;

  double x = target.x;
  double y = target.y;
  double teta = target.theta;
  //std::cout<<"x="<<std::to_string(x)<<" "<<"y="<<std::to_string(y)<<" "<<"teta="<<std::to_string(teta)<<std::endl;
  teta = fmod(teta + pi_,2*pi_);
  if (teta < 0){
    teta += 2*pi_;
  }
  teta= teta-pi_;

  DPxk.resize(2700);

  for (int k =0 ; k <= 2697; k=k+3)
  {
    
    DPxk(k) = x;
    DPxk(k+1) = y;
    DPxk(k+2) = teta;
  }
  flagOpt = 1;
  TargetFinale= {x, y, teta};
  flagMemorizzaTraiettoria=1;
}

//intercettazione traiettoria desiderata
void Controller::trajectoryCallback(const nav_msgs::Path trajectory){
  ScorriDPxk=0;

  double n_punti_traiettoria=trajectory.poses.size();
  
  DPxk.resize(n_punti_traiettoria*3);
  tempxk.resize(n_punti_traiettoria*3);

  double i=0;
  for(auto pose : trajectory.poses){
     double x = pose.pose.position.x;
     double y = pose.pose.position.y;
     double qz = pose.pose.orientation.z;
     double teta = -(2*asin(qz));
     tempxk(i)=x;
     tempxk(i+1)=y;
     tempxk(i+2)=teta;
     DPxk(i)=tempxk(0);
     DPxk(i+1)=tempxk(1);
     DPxk(i+2)=tempxk(2);
     i= i+3;
  }
  flagOpt = 1;
  flagTF=1;
  double xtf= tempxk((tempxk.size()-3));
  double ytf= tempxk((tempxk.size()-2));
  double tetatf= tempxk((tempxk.size()-1));
  TargetFinale={xtf, ytf, tetatf};
  flagMemorizzaTraiettoria=1;
}

void Controller::clockCallbackTEST(const rosgraph_msgs::Clock rclock)
{
  robot_clock=rclock.clock;
}

int main(int argc, char **argv)
{
  printf(".. creo rosnode mbnodecontrollerifopt \n");

  FtempiOttimizzazione.open ("OUT_alglib_TempiOttimizzazione.txt");
  FtempiCPU.open ("OUT_alglib_TempiCPU.txt");
  FdurataPercorso.open ("OUT_alglib_DurataPercorso.txt");
  FdistanzaPercorsa.open ("OUT_alglib_DistanzaPercorsa.txt");

  ros::init(argc, argv, "mbnodecontrolleralglib");
  ros::NodeHandle node;
  if (!ros::master::check())
    return (0);

  Controller controller(node);

  // ros::Subscriber subClock = node.subscribe("/clock", 1, &Controller::clockCallbackTEST, &controller);
  // LABORATORIO
  // ros::Subscriber subPioneer_pose = node.subscribe("/vrpn_client_node/epuck_robot_4/pose", 1, &Controller::ePuck_poseCallbackTEST, &controller);
  ros::Subscriber subPioneer_pose = node.subscribe("/ePuck2/pose", 1, &Controller::ePuck_poseCallbackTEST, &controller);
  ros::Subscriber subTarget = node.subscribe("/target", 1, &Controller::targetCallback, &controller);
  ros::Subscriber subTrajectory = node.subscribe("/trajectory", 1, &Controller::trajectoryCallback, &controller);
  
  ros::Timer timer1 = node.createTimer(ros::Duration(0.1), &Controller::timer1CallbackTEST, &controller);

	// 10 Hz
	ros::Rate loop_rate(10); 
  ros::spin();
 
  FtempiOttimizzazione.close();
  FtempiCPU.close();  
  FdurataPercorso.close();
  FdistanzaPercorsa.close();

  return 0;
}

// inizio funzione di costo
// -----mobRobCT0-----------------------------------------------------
Eigen::VectorXd mobRobCT0(Eigen::VectorXd x, Eigen::VectorXd u)
{
  // modello lineare tempo-continuo di un differential drive robot
  // 3 stati (x):
  // posizione del robot lungo x (x)
  // posizione del robot lungo y (y)
  // angolo di sterzo del robot (theta)
  // 2 inputs (u):
  // velocità lineare (v_r)
  // velocità angolare (w_r)
  double theta_r = x(2);
  Eigen::VectorXd dxdt = x;
  double v_r = u(0);
  double w_r = u(1);
  dxdt(0) = cos(theta_r) * v_r;
  dxdt(1) = sin(theta_r) * v_r;
  dxdt(2) = w_r;
  Eigen::VectorXd y = x;

  return dxdt;
};

double wrapToPi(double x)
{
  x = fmod(x + pi_, 2 * pi_);
  if (x < 0)
    x += 2 * pi_;
  return x - pi_;
};

// --------------------------------------------------------------------
Eigen::VectorXd mobRobDT0(Eigen::VectorXd xk, Eigen::VectorXd uk, double Ts)
{
  int M = 10;
  double delta = Ts / M;
  Eigen::VectorXd xk1 = xk;
  for (int i = 0; i < M; i++)
  {
    xk1 = xk1 + delta * mobRobCT0(xk1, uk);
  }

  // chiamata wrapToPi
  xk1(2) = wrapToPi(xk1(2));
  Eigen::VectorXd yk = xk;

  return xk1;
};

// --------------------------------------------------------------------
double mobRobCostFCN(Eigen::VectorXd du, Eigen::VectorXd x0, double Ts, int N, int M, Eigen::MatrixXd xref, Eigen::VectorXd u1)
{
  /* funzione di costo di NMPC per il controllo di robot mobili

  Inputs:
  du:     variabili di ottimizzazione, dal tempo k al tempo k+M-1 come vettore colonna
  x:      stato corrente al tempo k
  Ts:     tempo di campionamento del controllore
  N:      orizzonte di predizione
  M:      orizzonte di controllo
  xref:   stati del riferimento come una matrice le cui N+1 colonne ( dal tempo 0 al tempo N ) sono i riferimenti per il sistema
  u1:     precedente output del controllore al tempo k-1
  J:      costo della funzione oggetto

  */

  // la matrice Q penalizza le deviazioni dello stato dal riferimento
  Eigen::DiagonalMatrix<double, 3> Q(8, 10, 0.1);

  // matrice del costo terminale
  Eigen::DiagonalMatrix<double, 3> P = 100 * Q;

  // la matrice Rdu penalizza il rateo di cambiamento dell'input (du)
  Eigen::DiagonalMatrix<double, 2> Rdu(8, 8);

  // la amtrice Ru penalizza la grandezza dell'input (u)
  Eigen::DiagonalMatrix<double, 2> Ru(8, 8);

  // inizializzazione ref_k
  Eigen::VectorXd ref_k(3);
  ref_k << 0, 0, 0;

  // inizializzazione theta_d
  double theta_d = 0.0;

  // inizializzazione ek
  Eigen::VectorXd ek(3);
  ek << 0, 0, 0;

  // trasformazione di du in una matrice le cui righe sono du[k] da k a k+N, 
  // riempendo le righe dopo M con zero
  Eigen::MatrixXd alldu(N, 2);
  alldu.setZero(N, 2);
  int w = 0;
  for (int i = 0; i < M; ++i)
  {
    for (int c = 0; c < 2; ++c)
    {
      alldu(i, c) = du(w);
      w++;
    }
  }

  // calcolo del costo
  // inserimento stato iniziale e costo

  //xk = x0;
  Eigen::VectorXd xk(3);
  xk << 0, 0, 0;
  for (int i = 0; i < 3; i++)
  {
    xk(i) = x0(i);
  }

  //uk = u1;
  Eigen::VectorXd uk(2);
  uk << 0, 0;
  for (int i = 0; i < 2; i++)
  {
    uk(i) = u1(i);
  }

  //init J
  double j = 0;

  //costo primi N istanti 
  for (int i = 0; i < N; i++)
  {
    Eigen::VectorXd du_k(2);
    du_k << 0, 0;
    for (int j = 0; j < 2; j++)
    {
      du_k(j) = alldu(i, j);
    }
    du_k = du_k.transpose();
    uk = uk + du_k;

    for (int j = 0; j < 3; j++)
    {
      ref_k(j) = xref(i, j);
    }
    ref_k = ref_k.transpose();
    theta_d = ref_k(2);

    //Rot
    Eigen::MatrixXd Rot(3, 3);
    Rot.setZero(3, 3);
    Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;

    ek = Rot * (xk - ref_k);

    // calcolo costo
    j = j + ek.transpose() * Q * ek;
    j = j + du_k.transpose() * Rdu * du_k;
    j = j + uk.transpose() * Ru * uk;

    // ottengo stato al prossimo istante di predizione
    xk = mobRobDT0(xk, uk, Ts);
  }

  // costo terminale
  for (int j = 0; j < 3; j++)
  {
    ref_k(j) = xref(N, j);
  }

  ref_k = ref_k.transpose();

  theta_d = ref_k(2);

  Eigen::MatrixXd Rot(3, 3);
  Rot.setZero(3, 3);
  Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;
  ek = Rot * (xk - ref_k);

  j = j + ek.transpose() * P * ek;
  //std::cout << "J = " <<std::to_string(j) << std::endl;

  return j;
};

double FunzioneDiCosto(Eigen::VectorXd du)
{
  int M = 3;
  int N = 15;

  Eigen::VectorXd x0(3);
  x0 << G_posizione_iniziale[0], G_posizione_iniziale[1], G_posizione_iniziale[2];

  double Ts = 0.1;

  Eigen::MatrixXd xref(N + 1, 3);
  xref.setZero(N + 1, 3);

  // valorizzo il riferimento
  for (int i = 0; i < N + 1; ++i)
  {
    for (int c = 0; c < 3; ++c)
    {
      if (ScorriDPxk + c + 3 * i <= (DPxk.size() - 3))
      {
        xref(i, c) = DPxk(ScorriDPxk + c + 3 * i);
      }
      else
      {
        xref(i, c) = DPxk((DPxk.size() - 3) + c);
      }
    }
  }

  Eigen::VectorXd u1(2);
  u1 << G_vel_precedente[0], G_vel_precedente[1];

  double j = mobRobCostFCN(du, x0, Ts, N, M, xref, u1);
  return j;
};
// fine funzione di costo